let ObjectID = require('mongodb').ObjectID;

const getRows = (limit,offset_, cp)=>{
	const offset = offset_||0;
	return new Promise ((resolve,reject)=>{
		cp.count()
			.then(count=>{
				if(offset>=count){
					reject ({status:416,message:"Requested Range Not Satisfiable"})
				}
				cp.find({}).skip(offset).limit(limit).toArray((err,results)=>{
					if(err) throw err;
					const next  = {limit,offset:offset+limit  }
					resolve({
							count,
							next,
							previous: {limit,offset},
							results
						});
				})
			})
			.catch(err=>{
				reject(err);
			})
	})
}

const getRow = (id, cp)=>{
	return new Promise ((resolve,reject)=>{
		try{ObjectID(id);}catch(err){reject ({status:400,message:'wrong param'})}
		const _id =  ObjectID(id);
		cp.findOne({_id})
			.then(row=>{
				resolve(row);
			})
			.catch(err=>{
				reject({status:400,message: err.message});
			})
	})
}

const putRow = (id,value, cp)=>{
	return new Promise ((resolve,reject)=>{
		let row_;
		try{ObjectID(id);}catch(err){reject ({status:400,message:'wrong param'})}
		const _id =  ObjectID(id);
		cp.findOne({_id})
			.then(row=>{
				if(!row)return reject({status:404,message:"not found"});
				if(!validate(row,value)) return reject({status:400,message:'wrong param'});
				row_ = row;
				cp.updateOne({_id},{'$set': { value}})
				.then(rr=>{
					resolve(Object.assign({},row_,{value}))
					
				})
				.catch(err=>{
					reject(err)
				})
			})
			
			.catch(err=>{
				reject(err)
			})
	})
}



const validate=(row={},value)=>{
	const {type,choices} = row;
	switch(type){
		case 'text':
			return true;
		case 'bool':
			return value ==true || value ==false  ;
		case 'select':
			return choices.filter(item=>(item===value)).length>0;
		default:
			return false;
	}
}

module.exports = {getRows,getRow,putRow}