const router = require('express').Router(),
	services = require('./services');


module.exports = function (app,cp) {
	router.get('/fields', (req, res,next) => {
		const {limit,offset} = req.query;
		if(!checkParams(limit,offset)){
			return res.status(400).send('wrong query params');
		}
		services.getRows(parseInt(limit),parseInt(offset), cp)
			.then(rows=>{
				res.status(200).json(rows);
			})
			.catch(err=>{
				res.status(err.status ? err.status : 500).json(err.message ?err.message : err );
			})
	});

	router.get('/values/:id', (req, res,next)=>{
		const {id} = req.params;
		if(!id) return res.status(400).send('wrong params');
		services.getRow(id,cp)
			.then(row=>{
				res.status(200).json(row);
			})
			.catch(err=>{
				res.status(err.status ? err.status : 500).json(err.message ?err.message : err );
			})
	});
	router.put('/values/:id', (req, res,next)=>{
		const {id} = req.params;
		if(!id) return res.status(400).send('wrong params');
		if(req.body.value == 'undefined' || req.body.value == null)return res.status(400).send('wrong params');
		services.putRow(id,req.body.value,cp)
			.then(row=>{
				res.status(200).json(row);
			})
			.catch(err=>{
				res.status(err.status ? err.status : 500).json(err.message ?err.message : err );
			})
	})

	router.post('/values/:id', (req, res,next)=>{
		res.status(501).send('useles');
	})
	router.delete('/values/:id', (req, res,next)=>{
		res.status(501).send('useles');
	})

	return router;
};

const checkParams =(limit,offset)=>{
	if(!limit) return false;
	if(!parseInt(limit) ||parseInt(limit)<=0) return false;
	if(offset && parseInt(offset) && parseInt(offset)<0) return false;
	return true;
}