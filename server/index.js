let express  = require("express"),
	path = require("path"),
	webpack = require('webpack'),
	webpackDevMiddleware = require('webpack-dev-middleware'),
	webpackHotMiddleware = require('webpack-hot-middleware'),
	configs = require('../props'),
	bodyParser = require('body-parser'),
	MongoClient = require('mongodb').MongoClient,
	assert = require('assert'),
	config = require('../webpack.js')
	fs = require('fs');


let app = express();
app.use(bodyParser.json({limit:'100kb'}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static("./dist"));

app.get('/',function (req,res){
	res.sendFile(path.resolve('dist/index.html'));
});
MongoClient.connect('mongodb://localhost:27017/django', (err, db)=> {
  assert.equal(null, err);
  let collection = db.collection('django'); 
  require('./routes')(app, collection);
});
var compler = webpack(config);
app.use(webpackDevMiddleware(compler,{noInfo: true, publicPath: config.output.publickPath}));
app.use(webpackHotMiddleware(compler));

let server = app.listen(configs.port, err=>{
	if(err){
		console.log(err);
	}
	console.info('==> Listening on port %s. Open up http://127.0.0.1:%s/ in your browser.', configs.port, configs.port);
});




