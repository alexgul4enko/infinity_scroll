import React, { Component ,PropTypes} from 'react';
import { Provider } from 'react-redux';
import List from './components/List';
import './root.css'

const Root = props =>{
	return (
		<Provider store={props.store}>
			 <List/> 
		</Provider>
	)
}
Root.propTypes = {
  store: PropTypes.object.isRequired,
};

export default Root;