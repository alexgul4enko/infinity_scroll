export default function httpPromice(theUrl, type, data){
	return new Promise((resolve,reject)=>{
		var xmlHttp = new XMLHttpRequest();
		xmlHttp.addEventListener("error", ()=>{
			reject("error");
		}, false);
	    xmlHttp.onreadystatechange = () =>{ 
	        if (xmlHttp.readyState === 4 && xmlHttp.status === 200){
	        	resolve(JSON.parse(xmlHttp.responseText));
	        }
	        else if(xmlHttp.readyState === 4 && xmlHttp.status !== 200){
	        	reject(`${xmlHttp.status}:${xmlHttp.responseText||''}`);
	        }
	    }
	    xmlHttp.open(type, theUrl, true); 
	    xmlHttp.setRequestHeader('Content-Type', 'application/json; charset=utf-8')
	    xmlHttp.send(JSON.stringify(data));
	});
}