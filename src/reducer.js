import Constances from './Constances';
import {combineReducers}  from 'redux';
const rootReducer = combineReducers({
  fetch,
  list,
  load
});

function fetch  (state ={}, action){
	switch(action.type){
		case Constances.CHANGE_FETCH:
			return Object.assign({},state,action.rehidrate)
		default:
			return state;
	}
}

function list  (state =[], action){
	switch(action.type){
		case Constances.LOAD_MORE:
			return [...state, ...action.rehidrate];
		case Constances.SET_ANSWER:
			const {rehidrate} = action;
			console.log()
			return state.map(item=>{
				if(item._id == rehidrate._id){
					return rehidrate;
				}
				return item;
			});
		default:
			return state;
	}
}

function load  (state ={loading:false, err:''}, action){
	switch(action.type){
		case Constances.SWITCH_LOAD:
			const {loading,err} =state;
			return {loading:!loading, err};
		case Constances.SHOW_ERROR:
			return {loading:false, err:action.rehidrate}; 
		default:
			return state;
	}
}

export default rootReducer;