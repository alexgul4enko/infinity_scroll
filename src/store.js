import { createStore, compose, applyMiddleware } from 'redux';
import logger from 'redux-logger'
import thunk from 'redux-thunk';
import rootReducer from './reducer';

let finalCreateStore = compose(applyMiddleware(thunk, logger()))(createStore)

const initialState_ = {fetch:{next:{limit:50}}}

export default function configureStore (initialState = initialState_){
 let store = finalCreateStore(rootReducer,initialState);

 if (module.hot) {
    module.hot.accept('./reducer', () => {
      const nextReducer = require('./reducer').default; 
    });
  }
 return store;
}
