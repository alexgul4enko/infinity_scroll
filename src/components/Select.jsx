import React, { Component ,PropTypes} from 'react';

export default class Select extends Component{
	constructor(props){
		super(props);
		this.state={
			open:false,
			value : props.value
		}
		this.handleClick = this.handleClick.bind(this);
		this.open = this.open.bind(this);
		this.close = this.close.bind(this);
	}

	render(){
		return  <div style = {calcStyle(this.props.size)}>
					<span 
						onClick = {this.open} 
						style = {styles.title}
					>
						{this.state.value||this.props.items[0]}
					</span>
					{this.renderdata.call(this)}
				</div>
	}

	renderdata(){
		return this.state.open?
				(<ul style = {styles.ul}>
					{this.props.items.map((item,i)=>{
						return<div className = "hover-opacity"
									style = {styles.li} 
									onClick= {this.select.bind(this,item)}
									key={item+i}
								>
									{item}
								</div>
					})}
				</ul>):null;
				
	}

	select(val){
		this.close();
		this.props.onChange(this.props._id,val);
		this.setState({value:val});
	}

	handleClick(e){
		if(!e.target.className.includes("hover-opacity")){
			this.close();
		}
	}
	open() {
		this.setState({open:true});
		document.addEventListener('click',this.handleClick)
	}
	close() {
		this.setState({open:false});
		document.removeEventListener('click',this.handleClick)
	}
}

Select.propTypes={
	value:PropTypes.string,
	items:PropTypes.array,
	onChange: PropTypes.func,
}


const calcStyle = size=>({
	width:`calc(100% / 12 * ${size})`,
	height: 30,
	paddingBottom: "2px",
	borderBottom:'1px solid #394246',
	textAlign:'center',
	lineHeight:'30px',
	color:'#394246',
	fontSize:'1.4em',
	cursor:'pointer',
	position:'relative'
})

const styles={
	title:{
		width:'100%',
		height:"28px",
		display:'block'
	},
	ul:{
		position:'absolute',
		width:'100%',
		zIndex:6,
		left:0,
		top:30
	},
	li:{
		background:"#394246",
		width:'100%',
		height:"28px",
		opacity:0.9,
		color:'white',
		cursor:'pointer'


	}

}












