import React, { Component ,PropTypes} from 'react';

export default class DropDown extends Component{
	constructor(props){
		super(props);
		this.state={
			open:false,
		}
		this.handleClick = this.handleClick.bind(this);
		this.open = this.open.bind(this);
		this.close = this.close.bind(this);
	}

	render(){
		return  <div style = {styles.cont}>
					<span 
						onClick = {this.open} 
						style = {styles.title}
					>
						{this.props.value||this.props.items[0]}
					</span>
					{this.renderdata.call(this)}
				</div>
	}

	renderdata(){
		return this.state.open?
				(<ul style = {styles.ul}>
					{this.props.items.map(item=>{
						return<div className = "hover-opacity"
									style = {styles.li} 
									onClick= {this.select.bind(this,item)}
									key={item}
								>
									{item}
								</div>
					})}
				</ul>):null;
				
	}

	select(val){
		this.close();
		this.props.onChange(val);
	}

	handleClick(e){
		if(!e.target.className.includes("hover-opacity")){
			this.close();
		}
	}
	open() {
		this.setState({open:true});
		document.addEventListener('click',this.handleClick)
	}
	close() {
		this.setState({open:false});
		document.removeEventListener('click',this.handleClick)
	}
}

DropDown.propTypes={
	value:PropTypes.string,
	items:PropTypes.array,
	onChange: PropTypes.func,
}

const styles={
	cont:{
		width:100,
		height: 30,
		paddingBottom: "2px",
		borderBottom:'1px solid white',
		textAlign:'center',
		lineHeight:'30px',
		color:'white',
		fontSize:'1.4em',
		cursor:'pointer',
		position:'relative'
	},
	title:{
		width:'100%',
		height:"28px",
		display:'block'
	},
	ul:{
		position:'absolute',
		width:'100%',
		zIndex:6,
		left:0,
		top:30
	},
	li:{
		background:"#394246",
		width:'100%',
		height:"28px",
		border:'.5px solid white',
		opacity:0.9


	}

}












