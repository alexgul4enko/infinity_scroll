import React, { Component ,PropTypes} from 'react';

export default class Input extends Component {
	constructor(props){
		super(props);
		this.state = {
			value:props.value
		}
	}
	render(){
		return(
			<input 
				type='text' 
				style = {calcInputStyle(this.props.size)}
				onBlur = {this.handleChange.bind(this)}
				defaultValue = {this.props.value}
			/>
		)
	}

	handleChange(e){
		const {value} = e.target;
		this.props.onChange(this.props._id, value);
		this.setState({value});
	}
}

const calcInputStyle =size=>({
	width:`calc(100% / 12 * ${size})`,
	border:0,
	background:'transparent',
	borderBottom:'2px solid white',
	paddingBottom:'2px'
})