import React, { Component ,propTypes} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actions from '../actions';
import ListItem from './ListItem';
import DropDown from './DropDown';


const filters = ['Select type','bool','text','select'];

class List extends Component{
	constructor(props){
		super(props);
		this.state  = {
			filter:null
		}
		this.onScroll = this.onScroll.bind(this);
		this.filterChanged = this.filterChanged.bind(this);
	}
	render(){
		const {filter} = this.state;
		return (
			<div style = {styles.container}>
				<nav style = {styles.nav}>
					<DropDown
						value = {this.state.filter}
						items = {filters}
						onChange = {this.filterChanged}

					/>
				</nav>
				<ul style = {styles.ul} id = "list">
					{
						this.props.list.map(item=>{
							const {_id, type} = item;
							return (filter == type || filter == null)?
											<ListItem key = {item._id} {...item} change = {this.props.answer}/>:null
						})
					}
					<div key = 'loadingspin'>

					</div>

				</ul>
			</div>
		)
	}
	filterChanged(val){
		if(val == 'Select type'){
			this.setState({filter:null});
		}
		else{
			this.setState({filter:val});
		}
	}




	onScroll(e){
		const {offsetHeight,scrollTop,scrollHeight} = e.target;
		if( scrollTop + offsetHeight >= scrollHeight){
			this.props.loadMore();
		}
	}
	
	componentWillMount() {
		this.props.loadMore();
	}
	componentDidMount() {
		document.getElementById('list').addEventListener('scroll',this.onScroll)
	}
	shouldComponentUpdate(nextProps, nextState) {
		return nextProps.list.length != this.props.list.length ||
				nextState.filter != this.state.filter;
	}
	componentWillUnmount() {
		document.getElementById('list').removeEventListener('scroll',this.onScroll)
	}
}

List.defaultProps = {

}

List.propTypes = {
	
}

const styles = {
	container:{
		width:"100%",
		height:"100%",
		display:"flex",
		flexDirection:"column",
		alignItems:'center',
		background:"#AED9D4"
	},
	nav:{
		width:"calc(100% - 60px)",
		height:70,
		paddingLeft:30,
		paddingRight:30,
		background:"#394246",
		display:'flex',
		flexDirection:'row',
		flexWrap:'nowrap',
		justifyContent:'center',
		alignItems:'center'
	},
	ul:{
		width:"calc(100% - 40px)",
		height:"calc(100% - 70px)",
		maxWidth:500,
		paddingLeft:20,
		paddingRight:20,
		overflowY:'auto'
	},
	select:{
		width:100,
		height: 30,
		lineHeight:"30px",
		direction:'ltr',
		marginTop:40
	}
}


const mapStateToProps = (state= {})=> ({list:state.list})


const mapDispatchToProps = dispatch => bindActionCreators(actions,dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(List);