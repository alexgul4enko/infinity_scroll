import React, { Component ,propTypes} from 'react';
import Input  from './Input';
import Bool  from './Bool';
import Select from './Select';

export default class ListItem extends Component{
	constructor(props){
		super(props);
		this.handleChange =this.handleChange.bind(this);
	}
	render(){
		console.log('render')

		return (
			<div key = {this.props._id} style ={styles}>
				{this.props.question}
				<div>
					{this.renderContent(this.props)}
				</div>
			</div>
		)
	}
	shouldComponentUpdate(nextProps, nextState) {
		return nextProps.value != this.props.value;
	}
	renderContent(props){
		switch(props.type){
			case 'text':
				return <Input 
							{...props}
							onChange = {this.handleChange}
						/>
			case 'bool':
				return <Bool 
							{...props}
							onChange = {this.handleChange}
						/>
			case 'select':
				return <Select
						value = {props.value}
						items = {['',...props.choices]}
						onChange = {this.handleChange}
						{...props}

					/>
		}
	}

	handleChange(id,value){
		this.props.change({id, value})
	}
}






const styles ={
	width:'calc(100% - 20px)',
	padding:'10px',
	border:'1px solid white'
	
} 