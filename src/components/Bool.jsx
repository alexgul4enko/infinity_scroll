import React, { Component ,PropTypes} from 'react';

export default class Bool extends Component {
	constructor(props){
		super(props);
		this.state = {
			value:props.value
		}
	}
	render(){
		return(
			<button 
				className = 'material-icons'
				style = {calcInputStyle(this.props.size)}
				onClick = {this.handleChange.bind(this)}
			>
			{this.state.value ?'check_box':'check_box_outline_blank'}
			</button>
		)
	}

	handleChange(e){
		let value = this.state.value ?false:true;
		this.props.onChange(this.props._id, value);
		this.setState({value});
	}
}

const calcInputStyle =size=>({
	width:`calc(100% / 12 * ${size})`,
	border:0,
	background:'transparent',
	cursor:'pointer'
})

