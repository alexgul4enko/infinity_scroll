import Constances from './Constances';
import httpPromice from './httpPromice';
const actions = {
	loadMore:()=>{
		return (dispatch,getState)=>{
			dispatch(switchLoad);
			const {next} =getState().fetch;
			httpPromice(`/api/v1/fields?limit=${next.limit}${next.offset ? `&offset=${next.offset}`:''}`,"GET")
			.then(data=>{
				const {results,...rest}=data;
				dispatch(fetchresults(rest));
				dispatch(addListData(results));
				dispatch(switchLoad);
			})
			.catch(err=>{
				console.log(err)
				alert(err);//sorry for this
			})
		}
	},
	answer:rehidrate=>{
		return (dispatch,getState)=>{
			httpPromice(`/api/v1/values/${rehidrate.id}`,'PUT',{value:rehidrate.value})
			.then(item=>{
				console.log(item)
				dispatch(saveAnswer(item));
			})
			.catch(err=>{
				console.log(err)
				alert(err);//sorry for this
			})
		}
	}
}

const saveAnswer = rehidrate=>({
	type:Constances.SET_ANSWER,
	rehidrate
});
const showErr = rehidrate=>({
	type:Constances.SHOW_ERROR,
	rehidrate
});
const addListData = (rehidrate=[])=>({
	type:Constances.LOAD_MORE ,
	rehidrate
});

const fetchresults = (rehidrate={})=>({
	type:Constances.CHANGE_FETCH,
	rehidrate
});
const switchLoad=()=>({
	type:Constances.SWITCH_LOAD
})

export default actions;

