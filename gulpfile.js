var gulp = require('gulp');

gulp.task('copyHTML', function () {
    gulp.src('./client/*.html')
        .pipe(gulp.dest('./dist/'));
});

gulp.task('copfonts', function () {
    gulp.src('./client/fonts/google/*')
        .pipe(gulp.dest('./dist/fonts/google'));
});


gulp.task('default', ['copfonts','copyHTML']);