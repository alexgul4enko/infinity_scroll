
import React from 'react';
import { render } from 'react-dom';
import Root from '../src/App';
import configureStore from "../src/store";

render(
	  <Root store={configureStore()}/>,
	  document.getElementById('app'),
	);
