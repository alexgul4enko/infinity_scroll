const choises  = [
	'apple',
	'apricot',
	'avocado' ,
	'banana',
	'blackberry',
	'blackcurrant',
	'blueberry',
	'boysenberry' ,
	'cherry',
	'coconut',
	'fig',
	'grape',
	'grapefruit',
	'kiwifruit' ,
	'lemon',
	'lime',
	'lychee' ,
	'mandarin',
	'mango' ,
	'nectarine' ,
	'orange',
	'papaya',
	'peach' ,
	'pear',
	'pineapple',
	'plum',
	'pomegranate',
	'quince',
	'raspberry',
	'strawberry',
	'watermelon'
];
const questions = [
	'Whats your favourite girl group?',
	'What would be your ideal partner?',
	'Do you want children?',
	'Do you want a church wedding?',
	'Are you religious?',
	'Do you like reality TV programs?',
	'Do you like TV talent shows?',
	'If you were gay who would your life partner be?',
	'If you could go back in time to change one thing what would it be?',
	'How many hats do you own?',
	'Are you any good at pool?',
	'Whats the highest youve ever jumped into the water from?',
	'Have you ever been admitted to hospital?',
	'Have you ever had any brushes with the law?',
	'Have you ever been on TV?',
	'Have you ever met any celebrities?',
	'Have you ever been to Legoland?',
	'Have you ever done something heroic?',
	'Have you ever played a practical joke on anyone?',
	'Have you ever been the recipiant of a practical joke?',
	'What would be your best achievement to date?',
	'Do you prefer baths or showers?',
	'Do you prefer towel drying, blow drying or natural dryin your hair?',
	'Have you ever built a snowman?',
	'Have you ever been sledging?',
	'Have you ever flown a kite?',
	'What colour socks are you wearing?',
	'If you could live anywhere, where would that be?',
	'Have you ever been famous?',
	'Would you like to be a big celebrity?',
	'Would you ever go on Big Brother?',
	'How big is your TV?',
	'What is your most essential appliance?',
	'What type of music do you like?',
	'Have you ever been skinnydipping?',
	'How many Pillows do you sleep with?',
	'What position do you often sleep in?'
]

const getRandomQuestion = ()=>(questions[getRandom(0,36)])


const getRandomSelectAnswers= ()=>{
	let n = getRandom(3,6);
	let selects =[]
	while(n>0){
		selects.push(choises[getRandom(0,30)]);
		n--;
	}
	return selects;
}
const getRandom = (a=0,b)=>(Math.floor(Math.random()*(b-a))+a);
const createText =()=>({
	size: getRandom(3,12),
	type: "text",
	question: getRandomQuestion()
});
const createBool =()=>({
	size: getRandom(3,12),
	type: "bool",
	question: getRandomQuestion()
});
const createSelect =()=>({
	size: getRandom(3,12),
	type: "select",
	question: getRandomQuestion(),
	choices: getRandomSelectAnswers()
});



const getRandomRaw = ()=>{
	const n = getRandom(0,3);
	switch(n){
		case 0:
			return createText();
		case 1:
			return createBool();
		default:
			return createSelect();
	}
}



let arrr= [];
let j =1000000;
while(j>=0){
	arrr.push(getRandomRaw());
	j--;
}

let MongoClient = require('mongodb').MongoClient
  , assert = require('assert');
 

MongoClient.connect('mongodb://localhost:27017/django', (err, db)=> {
  assert.equal(null, err);
  let collection = db.collection('django'); 
  collection.insertMany(arrr, function(err, result) {
    assert.equal(err, null);
    db.close();
  });
  
});

